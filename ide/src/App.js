import React, {Component} from "react";

import {Container} from "@material-ui/core";
import BasicAppBar from './components/BasicAppBar.component'
import Home from './components/Home.component'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: <Home/>,
    };
  }

  async componentDidMount() { }

  render() {
    return (
        <Container maxWidth="xl">
          <BasicAppBar
              title="IDE"
          >
            {this.state.page}
          </BasicAppBar>
        </Container>
    );
  }
}

export default App;
