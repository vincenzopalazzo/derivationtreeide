import React from "react"
import {
    AppBar,
    BottomNavigation,
    BottomNavigationAction, Box,
    Container,
    IconButton,
    Toolbar,
    Typography,
} from "@material-ui/core"
import {PlayArrow, Menu} from "@material-ui/icons"
import theme from "../utils/Theme"

class BasicAppBar extends React.Component {
    render() {
        const {children, value, changeValue, title} = this.props
        return (
            <Container maxWidth="xl">
                <AppBar position="sticky" style={{
                    backgroundColor: theme.palette.background.paper
                }}>
                    <Toolbar>
                        <IconButton onClick={() => console.log("Click on menu icon")} disabled={true} edge="start"
                                    color="inherit" aria-label="menu">
                            <Menu/>
                        </IconButton>
                        <Typography style={{color: theme.palette.text.primary}} variant="h6">
                            {title}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Box m={5} mb={9}>
                    {children}
                </Box>
                <AppBar position="fixed" style={{
                    backgroundColor: theme.palette.background.paper,
                    width: "30%",
                    marginLeft: "35%",
                    marginRight: "35%",
                    borderTopRightRadius: "10px",
                    borderTopLeftRadius: "10px",
                    top: "auto",
                    bottom: 0,
                }}>
                    <BottomNavigation
                        style={{
                            borderRadius: "10px"}}
                        value={value}
                        onChange={(event, newValue) => console.log("Run code")}
                    >
                        <BottomNavigationAction label="Run" icon={<PlayArrow/>}/>
                    </BottomNavigation>
                </AppBar>
            </Container>
        )
    }
}

export default BasicAppBar;
