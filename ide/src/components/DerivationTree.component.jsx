import React from "react";
import MathJax from "mathjax3-react";

class DerivationTree extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            content: "\\begin{prooftree}\n" +
                "\\AxiomC{}\n" +
                "\\RightLabel{Hyp$^{1}$}\n" +
                "\\UnaryInfC{$P$}\n" +
                "\\AXC{$P\\to Q$}\n" +
                "\\RL{$\\to_E$}\n" +
                "\\BIC{$Q^2$}\n" +
                "\\AXC{$Q\\to R$}\n" +
                "\\RL{$\\to_E$}\n" +
                "\\BIC{$R$}\n" +
                "\\AXC{$Q$}\n" +
                "\\RL{Rit$^2$}\n" +
                "\\UIC{$Q$}\n" +
                "\\RL{$\\wedge_I$}\n" +
                "\\BIC{$Q\\wedge R$}\n" +
                "\\RL{$\\to_I$$^1$}\n" +
                "\\UIC{$P\\to Q\\wedge R$}\n" +
                "\\end{prooftree}"
        }
    }

    render() {
        return(
            <MathJax.Provider
                url="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"
                options={{
                    loader: {load: ['[tex]/bussproofs']},
                    tex: {packages: {'[+]': ['bussproofs']}}
                }}
            >
                <MathJax.Formula formula={this.state.content} />
            </MathJax.Provider>
        )
    }
}

export default DerivationTree;