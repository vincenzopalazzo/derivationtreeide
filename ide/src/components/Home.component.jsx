import React from "react";
import {Grid} from "@material-ui/core";
import IDEView from "./IDEView.component";
import DerivationTree from "./DerivationTree.component";

class Home extends React.Component {
    render() {
        return (<Grid container spacing={3}>
                    <Grid item xs={6}>
                        <IDEView />
                    </Grid>
                    <Grid item xs={6}>
                        <DerivationTree />
                    </Grid>
                </Grid>
        );
    }
}

export default Home;