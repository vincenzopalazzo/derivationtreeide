import React from "react";
import AceEditor from "react-ace";
// This is a bad hack made to support a cool theme,
// copy and past the monokai and change the propriety
import "../utils/GithubDark";
import "ace-builds/src-noconflict/mode-java";


class IDEView extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.onChange = this.onChange.bind(this);
    }

    onChange(newValue) {
        console.log("change", newValue);
    }

    render() {
        return (<AceEditor
            mode="java"
            theme="monokai"
            fontSize={16}
            height="80vh"
            width="50vw"
            onChange={this.onChange}
            name="UNIQUE_ID_OF_DIV"
        />);
    }
}

export default IDEView;