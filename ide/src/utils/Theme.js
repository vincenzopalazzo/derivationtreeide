import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#f9826c',
        },
        secondary: {
            main: '#f9826c',
        },
        background: {
            default: '#24292e',
            paper: '#2f363d',
        },
        text: {
            primary: '#8b9eb5',
        },
        divider: '#f9826c',
    },
});

export default theme
