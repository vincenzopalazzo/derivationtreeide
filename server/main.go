package main

import (
	"net/http"
	"strconv"
)

func init() {}

func compileProgram(w http.ResponseWriter, r *http.Request) {

}

func handleRequests(port int) error {
	http.HandleFunc("/", compileProgram)
	portStr := strconv.Itoa(port)
	return http.ListenAndServe(":" + portStr, nil)
}

func main() {
	handleRequests(3001)
}
